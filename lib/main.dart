import 'package:flutter/material.dart';
import 'package:outlook_redesign/screens/main/main_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AfroMail',
      theme: ThemeData(),
      home: MainScreen(),
    );
  }
}
