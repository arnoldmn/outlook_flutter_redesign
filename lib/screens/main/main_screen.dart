import 'package:flutter/material.dart';
import 'package:outlook_redesign/components/side_menu.dart';
import 'package:outlook_redesign/responsive.dart';
import 'package:outlook_redesign/screens/email/mail.dart';

import 'components/listOfEmails.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    return Scaffold(
        body: Responsive(
      mobile: ListOfEmails(),
      tablet: Row(
        children: [
          Expanded(
            flex: 6,
            child: ListOfEmails(),
          ),
          Expanded(
            flex: 9,
            child: EmailScreen(),
          ),
        ],
      ),
      desktop: Row(
        children: [
          Expanded(
            flex: _size.width > 1340 ? 2 : 4,
            child: SideMenu(),
          ),
          Expanded(
            flex: _size.width > 1340 ? 3 : 5,
            child: ListOfEmails(),
          ),
          Expanded(
            flex: _size.width > 1340 ? 8 : 10,
            child: EmailScreen(),
          ),
        ],
      ),
    ));
  }
}
